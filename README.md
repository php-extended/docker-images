# docker-images

Docker Images to run CI on other packages

Inspirations :

- https://github.com/Burnett01/docker-php7-1-apache-mysql/blob/master/Dockerfile
- https://github.com/TetraWeb/docker/blob/master/php/7.0/Dockerfile
- https://github.com/thecodingmachine/docker-images-php/blob/v3/Dockerfile.slim.cli
- https://github.com/docker-library/php
